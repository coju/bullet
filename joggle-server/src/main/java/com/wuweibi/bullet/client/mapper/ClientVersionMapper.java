package com.wuweibi.bullet.client.mapper;

import com.wuweibi.bullet.client.entity.ClientVersion;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author marker
 * @since 2021-08-12
 */
public interface ClientVersionMapper extends BaseMapper<ClientVersion> {

}
